let db;

export function setDB( database ) {
	db = database;
	return db;
}


export function getAllContacts() {
	return !db
		? Promise.reject('no database!')
		: db.allDocs({ include_docs: true, conflicts: true })
			.then(docs => docs.rows.map(row => row.doc))
}

export function getFavourites() {
	return !db
		? Promise.reject('no database!')
		: db.allDocs({ include_docs: true })
			.then(docs => docs.rows
				.map(row => row.doc)
				.filter(contact => contact.isFavourite)
			)

}

export function addContact ( contact ) {
	if (!contact) throw new TypeError('Trying to add empty contact data')
	contact.createdAt = Date.now()
	contact.isFavourite = false
	return db.post(contact)
}

export function getContact ( id ) {
	return !db
		? Promise.reject('no database!')
		: db.get(id, {revs: true, revs_info: true})
}

export function updateContact( contact ) {
	console.log('update contact')
	return !db
		? Promise.reject('no database!')
		: db.put(contact)
}

export function deleteContact ( contact ) {
	return !db
		? Promise.reject('no database!')
		: db.remove(contact)
			.catch(err => console.warn('remove failed', err))
}

