import { goto } from '@sapper/app'
import { messages } from '../stores'

// default sort of booleans is true first
export function sortBy(list, prop, reverse = false) {
	if (!list.length) return list
	return list.slice().sort((a, b) => {
		const aVal = typeof a[prop] === 'string' ? a[prop].toLowerCase() : a[prop];
		const bVal = typeof b[prop] === 'string' ? b[prop].toLowerCase() : b[prop];
		if (typeof aVal === 'string') return reverse
			? bVal.localeCompare(aVal)
			: aVal.localeCompare(bVal)
		if (typeof aVal === 'boolean') {
			if (reverse) return (aVal === bVal) ? 0 : aVal ? 1 : -1
			return (aVal === bVal) ? 0 : aVal ? -1 : 1
		}
		return reverse
			? aVal < bVal
			: aVal > bVal
	})
}

export function filterBy(list, filter, fuzzy) {
	if (!list || !filter) return list;

	const [ filterKey, filterValue ] = filter.split(':')
	return list.filter(item => {
		if (!filterKey || !filterValue) return true
		if (typeof item[filterKey] !== 'string') return item[filterKey] === filterValue
		else return new RegExp(filterValue, 'i').test(item[filterKey])
	})
}

export function showMessage(message, url, cb) {
	messages.update(n => message)
	setTimeout(() => {
		messages.update(n => null)
		if (url) goto(url)
		if (cb) cb()
	}, 800)
}