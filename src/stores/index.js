import { writable } from 'svelte/store'

export const messages = writable(null)

export const dbMessages = writable(null)