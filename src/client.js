import * as sapper from '@sapper/app';
import PouchDB from 'pouchdb-browser';

import { setDB, addContact } from './routes/_database.js'
import { dbMessages } from './stores'

const DB_URL = "https://4317da34-4038-4375-bb18-5552402fc9a5-bluemix.cloudantnosqldb.appdomain.cloud/ida-contacts"
const API_KEY = "bmiLQMG4rCGzfJGzgpyVu40D6Z3oOAfdXhVBuNLbmejZ"
const auth = {
	username: "erisonstallikindiffecate",
	password: "db392df76f46530bd13157b3887b1f7edad3a409"
}

setDB(new PouchDB('ida-contacts'))
const sync = PouchDB.sync('ida-contacts', DB_URL, {
	live: true,
	retry: true,
	auth
})
.on('change', info => {
	if (info.direction === "pull") dbMessages.update(n => "db updated")
})
.on('error', err => console.error('DB PROBLEM', err))

sapper.start({
	target: document.querySelector('#sapper')
})